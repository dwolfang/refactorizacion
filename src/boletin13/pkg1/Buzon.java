/*
 * Define a clase Buzon para xestionar unbuzon de correo electrónicos ordenados segundo a orde de chegada.
Para representar o buzon de correo úsase un array de correos electrónicos; estes, á súa vez, son obxectos da clase Correo1
e teñen como atributos un String, co contido do correo, e un indicativo para saber se foron lidos ou non.
Define a clase Correo1 cos métodos que creas convenientes, sabendo que a clase Buzon debe incluír os seguintes métodos públicos:
a) int numeroDeCorreos(), que calcula cantos correos hai no buzon de correo
b) void engade (Correo1 c), que engade c ao buzon
c) boolean porLer(), que determina se quedan correos por ler
d) String amosaPrimerNoLeido(), que amostra o primeiro correo non lido
e) String amosa(int k), que amostra o correo k-ésimo, fora lido ou non
f) void elimina(int k), que elimina o correo k-ésimo.

 */
package boletin13.pkg1;

import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;

/**
 *
 * @author Luis
 */
public class Buzon {

    /**
     * método para ver o número de correos que hai no buzón
     * @param email ArrayList de correos
     * @return o numero de correos que hai no buzón
     */
    public int numeroDeCorreos(ArrayList<Correo1> email){
        return email.size();
        
    
    }
    public float pruebaReestructurar(ArrayList<Correo1> emailA){
        
        return emailA.size();}
    /**
     * método para engadir un correo o buzón
     * @param email ArrayList de correos
     */
    public void engade(ArrayList<Correo1> email){
        email.add(new Correo1(contidoCorreo()));
        saida("Correo engadido na posición "+email.size());
    }
    /**
     * método para saber si quedan correos sin ler
     * @param email ArrayList de correos
     * @return true si quedan correos por ler e false se non hai correos sen ler
     */
    public boolean porLer(ArrayList<Correo1> email){
        boolean aux = false;
        for(int i=0;i<email.size();i++){
            if(email.get(i).isLido()==false){
                aux = true;
            }
        }
        return aux;
    }
    public int cantosNonLidos(ArrayList<Correo1> email){
        int contador = 0;
        for(int i=0;i<email.size();i++){
            if(email.get(i).isLido()==false){
                contador++;
            }
        }
        return contador;
    }
    /**
     * método para ver o contido do primeiro correo non lido
     * @param email ArrayList de correos
     * @return O primeiro correo non lido
     */
    public String amosaPrimerNoLeido(ArrayList<Correo1> email){
        String aux = "";
        for(int i=0;i<email.size();i++){
            if(email.get(i).isLido()==false) {
                aux = email.get(i).getContido();
                email.get(i).setLido(true);
                break;
            }else{
                aux = "Todas as mensaxes están lidas!";
            }
        }
        return aux;
        
    }
    /**
     * método para mostrar un correo concreto según o numero de orde do correo
     * @param k o número de correo que queremos mostrar
     * @param email ArrayList de correos
     * @return o contido do correo que pedimos mostrar
     */
    public String amosa(int k, ArrayList<Correo1> email){
        email.get(k).setLido(true);
        return email.get(k).getContido();
    }
    /**
     * método para eliminar un correo concreto según o número de orde do correo
     * @param k o número de correo que queremos eliminar
     * @param email ArrayList de correos
     */
    public void elimina(int k,ArrayList<Correo1> email){
        saida("Eliminado o correo \""+email.get(k).getContido()+"\" que estaba na posición "+(k+1));
        email.remove(k);
    }
    /**
     * método para preguntar o número de correo
     * @return o número de correo tecleado polo usuario
     */
    public static int introducirNumero(){
        String resposta = JOptionPane.showInputDialog("Introduce o número de correo:");
        return Integer.parseInt(resposta)-1;
    }
    /**
     * método para mostrar por pantalla o parámetro que lle enviamos
     * @param saida Un String que será a menxase que vai aparacer por pantalla
     */
    public static void saida(String saida){
        JOptionPane.showMessageDialog(null, saida);
    }
    /**
     * método para que o usuario introduza o contido do correo
     * @return o contido do correo
     */
    public static String contidoCorreo(){
        return JOptionPane.showInputDialog("Contido do correo:");
    }
    /**
     * método para eliminar todos os correos lidos
     * @param email ArrayList de correos
     */
    public void eliminarTodoLido(ArrayList<Correo1> email){
        int i= 0;
        int contador = 0;
        Iterator it = email.iterator();
        while(it.hasNext()){
            if(email.get(i).isLido()==true){
                email.remove(i);
                i--;
                contador++;
            }
            i++;
        }
        saida("Eliminados "+contador+" correos");
    }
    
    /**
     * Aplicación para xestionar un buzón de correos
     * @param args 
     */
    public static void main(String[] args) {
        ArrayList<Correo1> email = new ArrayList<>();
        Buzon obx = new Buzon();
        Correo1 correo = new Correo1("Proba!!!!", false);
        String resposta;
        int opcion;
        do{
            resposta = JOptionPane.showInputDialog("Teclee a opción que desexe: "
                    + "\n 1. Engadir correo"
                    + "\n 2. Ver número de correos"
                    + "\n 3. Saber si hai correos non lidos"
                    + "\n 4. Número de correos non lidos"
                    + "\n 5. Ver 1º correo non lido"
                    + "\n 6. Ver correo (número de correo introducido polo usuario)"
                    + "\n 7. Eliminar correo (número de correo introducido polo usuario)"
                    + "\n 8. Eliminar todos os correos lidos"
                    + "\n 0. Finalizar a aplicación");
            opcion = Integer.parseInt(resposta);
            switch(opcion){
                case 0:break;
                case 1:obx.engade(email);break;
                case 2:saida("Número de correos: "+obx.numeroDeCorreos(email));break;
                case 3:
                    if(obx.porLer(email)){
                        saida("Quedan mensaxes sen ler!");
                    }else {
                saida("Todas as mensaxes estan lidas!");
            }
                    break;
                case 4:saida("Mensaxes non lidas: \n"+obx.cantosNonLidos(email));break;
                case 5:saida(obx.amosaPrimerNoLeido(email));break;
                case 6:
                    try{
                        saida(obx.amosa(introducirNumero(), email));
                    }catch(IndexOutOfBoundsException e){
                        saida("Non existe ese número de correo!"
                                + "\nNo buzón hai "+email.size()+" correos");
                    }
                    break;
                case 7:
                    try{
                    obx.elimina(introducirNumero(), email);
                    }catch(IndexOutOfBoundsException e){
                        saida("Non existe ese número de correo!"
                                + "\nNo buzón hai "+email.size()+" correos");
                    }
                    break;
                case 8:obx.eliminarTodoLido(email);break;
                default:saida("Teclea unha opción correcta!");
            }
        
        }while(opcion!=0);
    }
}
