
package boletin13.pkg1;

/**
 *
 * @author Luis
 */
public class Correo {
    private String contido;
    private boolean lido;

    public Correo() {
    }

    public Correo(String contido, boolean lido) {
        this.contido = contido;
        this.lido = lido;
    }
    public Correo(String contido){
        this.contido = contido;
        lido = false;
    }

    /**
     * @return the contido
     */
    public String getContido() {
        return contido;
    }

    /**
     * @param contido the contido to set
     */
    public void setContido(String contido) {
        this.contido = contido;
    }

    /**
     * @return the lido
     */
    public boolean isLido() {
        return lido;
    }

    /**
     * @param lido the lido to set
     */
    public void setLido(boolean lido) {
        this.lido = lido;
    }
    
    
}
